import os
#import filecmp

os.system("python3 Python/PythonHeterodyning.py")
os.system("cd C && make threaded")
os.system("cd C && make run_threaded")

f1 = open("/home/pi/Documents/UCT/EEE3096S/GitCopy2/EEE3096S-2023/WorkPackage1/Python/result.txt", "r")
f2 = open("/home/pi/Documents/UCT/EEE3096S/GitCopy2/EEE3096S-2023/WorkPackage1/C/result.txt", "r")

f1_data = f1.readlines()
f2_data = f2.readlines()

i=0

for line1 in f1_data:
    i += 1

    for line2 in f2_data:

    if line1==line2:
            print("Line ", i, ": IDENTICAL")
    else:
            print("\tFile 1:", line1, end='')
            print("\tFile 2:", line2, end='')
        break

f1.close()
f2.close()

#result = filecmp.cmp(f1,f2, shallow=False)
#print(result)

