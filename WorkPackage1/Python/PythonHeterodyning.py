#!/usr/bin/python3
"""
Python Practical Code for heterodyning and performance testing
Keegan Crankshaw
Date: 7 June 2019

This isn't necessarily performant code, but it is Pythonic
This is done to stress the differences between Python and C/C++

"""

# import Relevant Librares
import Timing
from data import carrier, data
import numpy

# Define values.
c = carrier
d = data
result = []

# Main function
def main():
    print("There are {} samples".format(len(c)))
    print("using type {}".format(type(data[0])))
    Timing.startlog()
    for i in range(len(c)):
        result.append(c[i] * d[i])
    Timing.endlog()
#    print("result array:\n", result)

    result_file_path = "/home/pi/Documents/UCT/EEE3096S/GitCopy2/EEE3096S-2023/WorkPackage1/Python/result.txt"

    num_vals_to_write = 2589
    numpy.savetxt(result_file_path, result[:num_vals_to_write], fmt="%.6f")


# Only run the functions if this module is run
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Exiting gracefully")
    except Exception as e:
        print("Error: {}".format(e))
